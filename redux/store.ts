import {
  configureStore,
  createSlice,
  PayloadAction,
  ThunkAction,
} from "@reduxjs/toolkit";
import { Action } from "redux";
import { createWrapper, HYDRATE } from "next-redux-wrapper";
import { State } from "../types";
import { showErrorToast } from "../helpers";

const initialState: State = {
  account: null,
  loading: false,
  error: null
};

export const { reducer, actions } = createSlice({
  name: "store",
  initialState,
  reducers: {
    connectRequest: (state, action) => {
      return {
        ...state,
        loading: true,
        error: null,
      }
    },
    connected: (state, action: PayloadAction<Partial<State>>) => {
      return {
        ...state,
        ...action.payload,
        error: null,
        loading: false,
      }
    },
    connectFailed: (state, action: PayloadAction<Partial<State["error"]>>) => {
      return {
        ...state,
        error: action.payload,
        loading: false
      }
    },
    disconnect: (state, action) => {
      return {
        ...state,
        account: null,
        loading: false,
        error: null
      }
    },
  },
});

const makeStore = () =>
  configureStore({
    reducer,
    devTools: true,
  });

export type AppStore = ReturnType<typeof makeStore>;
export type AppState = ReturnType<AppStore["getState"]>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  AppState,
  unknown,
  Action
>;

export const wrapper = createWrapper<AppStore>(makeStore);
