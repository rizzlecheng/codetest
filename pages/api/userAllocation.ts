import type { NextApiRequest, NextApiResponse } from "next";
import { PresaleRound } from "../../types";

export type AllocationData = {
  res: { tokenAmount: number; presaleRounds: PresaleRound[] } | null;
};

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<AllocationData>
) {
  res.status(200).json({ res: null });
}
